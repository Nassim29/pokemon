<?php
use Migrations\AbstractMigration;

class RemoveUsernameFromComments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('comments');
        $table->removeColumn('username');
        $table->update();
    }
}
